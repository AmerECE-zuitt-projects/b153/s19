// Exponent Operator
let firstNum = 8 ** 2;
console.log(firstNum);
let secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literals
let song = "Careless Whisper";
let artist = "George Michael";
console.log(`My favorite song is ${song} by ${artist}`);

// Object Destructuring
let person = {
    firstName: "John",
    middleName: "Jacob",
    lastName: "Smith"
};
let {firstName, middleName, lastName} = person;
console.log(firstName);
console.log(middleName);
console.log(lastName);

// Using the .keys() method to loop through objects. Create an array populated with the keys/properties of the object passed to its parameter.
const personKeys = Object.keys(person);
console.log(personKeys);

personKeys.forEach((key) => {
    console.log(typeof person[key]);
});

// Array Destructure
const people = ["John", "Joe", "Jack"];
let [firstPerson, secondPerson, thirdPerson] = people;
console.log(firstPerson);
console.log(secondPerson);
console.log(thirdPerson);

/* Arrow Functions
    - Compact alternative syntax to writing traditional functions 
    - The parentheses around the argument can be removed if exactly one argument is needed for the function.
    - The curly braces can be removed if your function needs to execute a single statement.

*/
const sayHello = () => {
    console.log("Hello!");
};
// function addNum (num) {
//     return num + num;
// };
const addNum = num => num + num;

// const addNum = (num) => {
//     return num + num;
// };

// Arrow function have what are called "implicit return statements" where the keyword return is not necessary, 
const add = (x, y) => x + y;

