// ------------ Cube Number ------------
const getCube = x => {
    console.log(`The cube of ${x} is ${x ** 3}`);
};
getCube(2);

// ------------ Address Array ------------
let address = [
    "258 Washington Ave NW",
    "California",
    "90011"
];
let [avenue, city, zipCode] = address;
console.log(`I live at ${avenue}, ${city} ${zipCode}.`);

// ------------ Animal Object ------------
let animal = {
    name: "Lolong",
    type: "crocodile",
    weight: 1075,
    measurement: "20 ft 3 in"
};
let {name, type, weight, measurement} = animal;
console.log(`${name} was a saltwater ${type}. He weighted at ${weight} kgs with a measurement of ${measurement}.`);

// ------------ Array of Numbers ------------
let numbers = [1, 2, 3, 4, 5];
numbers.forEach(number => console.log(number));

// ------------ Reduce Number ------------
let reduceNumber = numbers.reduce((accumulator, currentValue) => {
    return accumulator + currentValue;
});
console.log(reduceNumber);

